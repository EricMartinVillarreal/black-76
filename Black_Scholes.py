import numpy as np
import scipy.stats as si
import scipy.optimize as si_opt
import scipy.interpolate as si_i


def euro_vanilla_call(S, K, T, r, sigma):

    """
    :param S: Spot
    :param K: Strike
    :param T: Time to maturity
    :param r: interest rate
    :param sigma: volatility
    :return:
    """
    d1 = (np.log(S/K) + (r + 0.5*sigma**2)*T)/(sigma*np.sqrt(T))
    d2 = (np.log(S/K) + (r - 0.5*sigma**2)*T)/(sigma*np.sqrt(T))

    call = (S*si.norm.cdf(d1, 0.0, 1.0) - K*np.exp(-r*T)*si.norm.cdf(d2, 0.0, 1.0))
    return call


def black76_call(sigma, future_price, strike, delivery, risk_free):
    """
    :param future_price: Future price of the commodity
    :param strike: Strike Price of the future
    :param delivery: Delivery date of the future
    :param risk_free: The risk free rate
    :param sigma: The implied volatility
    :return: price of the call
    """
    d1 = (np.log(future_price/strike)+((sigma**2)/2)*delivery)/(sigma*np.sqrt(delivery))
    d2 = d1 - (sigma*np.sqrt(delivery))

    call = np.exp(-risk_free*delivery)*(future_price*si.norm.cdf(d1, 0.0, 1.0) - strike*si.norm.cdf(d2, 0.0, 1.0))
    return round(call, 4)


def black76_put(sigma, future_price, strike, delivery, risk_free):
    """
    :param future_price:
    :param strike:
    :param delivery:
    :param risk_free:
    :param sigma:
    :return:
    """
    d1 = (np.log(future_price / strike) + ((sigma ** 2) / 2) * delivery) / (sigma * np.sqrt(delivery))
    d2 = d1 - (sigma * np.sqrt(delivery))

    put = np.exp(-risk_free*delivery)*(strike*si.norm.cdf(-d2, 0.0, 1.0) - future_price*si.norm.cdf(-d1, 0.0, 1.0))
    return put


def black76_iv_call(future_price, strike, delivery, risk_free, call):
    """
    :param future_price: Future price of the commodity
    :param strike: Strike Price of the future
    :param delivery: Delivery date of the future
    :param risk_free: The risk free rate
    :param call: The price of the call option
    :return: Implied volatility
    """
    constants = (future_price, strike, delivery, risk_free, call)

    def y(sigma, c1, c2, c3, c4, c5):
        return abs(black76_call(sigma, c1, c2, c3, c4)-c5)
    return si_opt.OptimizeResult.get(si_opt.minimize_scalar(y, args=constants), "x")


def black76_iv_put(future_price, strike, delivery, risk_free, put):
    """
    :param future_price: Future price of the commodity
    :param strike: Strike price of the commodity
    :param delivery: delivery date of the future
    :param risk_free: the risk free rate
    :param put: The price of the put option
    :return:
    """
    constants = (future_price, strike, delivery, risk_free, put)

    def y(sigma, c1, c2, c3, c4, c5):
        return abs(black76_put(sigma, c1, c2, c3, c4) - c5)

    return si_opt.OptimizeResult.get(si_opt.minimize_scalar(y, args=constants), "x")

