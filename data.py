from Black76.Black_Scholes import *
import pandas as pd
import yfinance as yf
import matplotlib.pyplot as plt
import sympy as sym


def get_wti_price():
    wti = yf.Ticker("CL=F")
    wti_price_data = pd.DataFrame(wti.history(period="end", interval="1m"))
    wti_latest_price = wti_price_data.iloc[[-1], [0]]
    return round(float(wti_latest_price.iloc[0, 0]), 5)


# Retrieves the most recent WTI Crude Price and assigns to WTI_price
WTI_price = get_wti_price()

dd = 2/12
rf = 0.02
fp = 58

# Imports Future Data for WTI from CSV
wti_data = pd.read_csv("full_options.csv")
wti_data = pd.DataFrame(wti_data)
wti_data = wti_data.iloc[:, [0, 3]]

wti_data = wti_data.dropna(0, how='any')


# Divides WTI Future Data into calls and puts
WTI_call_only = wti_data[wti_data['Strike'].str.endswith("C")]
WTI_put_only = wti_data[wti_data['Strike'].str.endswith("P")]


# Removes Put and Call specification on the strike price
WTI_call_only.loc[:, 'Strike'] = WTI_call_only['Strike'].str.slice(0, -1)
WTI_put_only.loc[:, 'Strike'] = WTI_put_only['Strike'].str.slice(0, -1)


# Converts strike prices to floats
WTI_call_only.loc[:, 'Strike'] = WTI_call_only['Strike'].astype(float)
WTI_put_only.loc[:, 'Strike'] = WTI_put_only['Strike'].astype(float)


# Adds Implied Volatility column to WTI strike tables
call_iv = []
for i in range(len(WTI_call_only['Last'])):
    print(str(i+1) + "/" + str(len(WTI_call_only)))
    call_iv.append(black76_iv_call(fp, WTI_call_only.iloc[i, 0], dd, rf, WTI_call_only.iloc[i, 1]))

WTI_call_only.loc[:, 'IV'] = call_iv

#WTI_call_only = WTI_call_only[WTI_call_only['IV'] >= 0.01]
#WTI_call_only = WTI_call_only[WTI_call_only['IV'] <= 0.8]

WTI_call_only = WTI_call_only[WTI_call_only['Strike'] >= 38]
WTI_call_only = WTI_call_only[WTI_call_only['Strike'] <= 85]

call_iv_curve_poly = np.polyfit(WTI_call_only['Strike'], WTI_call_only['IV'], 2)


def curve_fit(x):
    deg = len(call_iv_curve_poly) - 1
    ans = 0

    for d in range(deg):
        ans = ans + call_iv_curve_poly[d]*x**(deg-d)
    y = ans + call_iv_curve_poly[deg]

    return y


def call_price(x):
    y = []

    for h in range(len(x)):
        y.append(black76_call(curve_fit(x[h]), fp, x[h], dd, rf))
    return y


def derive_cdf(x):
    d1 = (np.log(fp/x) + ((curve_fit(x) ** 2) / 2) * dd) / (curve_fit(x) * np.sqrt(dd))
    d2 = d1 - (curve_fit(x) * np.sqrt(dd))
    return -np.exp(-rf*dd)*si.norm.cdf(d2, 0.0, 1.0)


def derive_pdf(x):
    y = np.diff(x)

    z = (1 / sum(y))
    return y*z


def calculate_con_range():

    return


X = np.linspace(0, WTI_call_only['Strike'].max(), 10000)
print(sum(derive_pdf(derive_cdf(X))))

fig, axes = plt.subplots(3)
axes[0].plot(WTI_call_only['Strike'], WTI_call_only['IV'])
axes[0].plot(X, curve_fit(X))
axes[0].set_ylim(0.2, WTI_call_only['IV'].max())
axes[0].set_xlim(WTI_call_only['Strike'].min(), WTI_call_only['Strike'].max())
axes[0].set_xlabel('Strike Price')
axes[0].set_ylabel('Implied Volatility')

axes[1].plot(X, call_price(X))
axes[1].set_xlabel('Strike Price')
axes[1].set_ylabel('Call Price')

axes[2].plot(X[:len(X)-1], derive_pdf(derive_cdf(X)))
axes[2].set_xlabel('Commodity Price')
axes[2].set_ylabel('Probability')


#plt.figure(4)
#plt.plot(X[:len(X)], derive_cdf(X))
#plt.xlabel('Strike Price')
#plt.ylabel('Cumulative Probability')
plt.show()

